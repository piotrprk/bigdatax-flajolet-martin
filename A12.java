import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

class FM{
	ArrayList<Integer> stream;
	String SPLIT_BY = ", ";
	
	public FM() {
		this.stream = new ArrayList<>();		
	}
	public FM(String file_name) {
		this.stream = new ArrayList<>();
		this.loadDataRaw(file_name);
	}

	public void loadDataRaw(String file_name) {
		// get the data
		File f = new File(file_name);

		try (Scanner scanner = new Scanner(f)){	
			while(scanner.hasNextLine()) {
				String[] tokens = scanner.nextLine().split("\n");
				// read line by line
				for(String i:tokens) {
					// split line
					String[] line = i.split(SPLIT_BY);	// get data by split by TAB
					// add file text line as basket
					this.addData(line);
				}	
			}
		} catch (FileNotFoundException ex) {
			System.out.println(ex);
		}			
	}
		
	private void addData(String[] line) {
		for (String s:line) {
			this.stream.add( Integer.valueOf(s) );	
		}		
	}
	public void printStream() {
		System.out.println(this.stream);
	}
	
	// hash function concept
	// y = a*x + b mod c
	public int hx(int x, int a, int b, int c) {
		return (a*x + b) % c;
	}
	// hash function
	public int h(int x){
		return (53 * x + 7) % 1048576;
	}
	// hash function for larger integers
	public long h2(int x, int len){
		return (53 * x + 7) % (long)Math.pow(2, (len-1));
	}	
	// len - binary length
	public String intToBinary(int x, int len) {
		// set binary length to 2^len > x
		if (Math.pow(2, len) < x)
			return "ERROR";
		StringBuilder binary = new StringBuilder();
       int shift = len - 1;
       while (shift >= 0) {
    	   // shift x
    	   int bit = ( x >> shift ) & 1;
    	   binary.append(bit);
    	   shift--;
       }		
		return binary.toString();
	}	
	//
	// use long integer for bigger numbers
	public String hashAndBinary(int x, int len) {
		// use hash2 with length as param
		long hash = h2(x, len);
		// set binary length to 2^len > x
		if (Math.pow(2, len) < hash)
			return "ERROR";
		StringBuilder binary = new StringBuilder();
       int shift = len - 1;
       while (shift >= 0) {
    	   // shift x
    	   long bit = ( hash >> shift ) & 1;
    	   binary.append(bit);
    	   shift--;
       }		
		return binary.toString();
		
	}
	
	// hash every element of a stream
	public void hashAll() {
		for (int i:this.stream) {
			String result = intToBinary(this.h(i), 20);
			System.out.println(i + " -> " + this.h(i) + " -> "+ result);			
		}
	}
	
	// get tail length
	// count 0 at the end of string
	public int tailLength(String binary) {
		String[] chars = binary.split("(?!^)");
		int counter = 0;
		for (int i=chars.length-1; i >= 0; i--) {
			if (chars[i].equals("0") )
				counter++;
			else if (chars[i].equals("1") )
				break;
		}
		return counter;
	}
	// count tail lengt with String.lastIndexOf function
	public int tailLength2(String binary) {		
		int i = binary.lastIndexOf("1");		
		return binary.length() - i - 1;
	}
	
	public int maxT(int from, int to) {
		int max = 0;
		int max_i = 0;
		for (int i=from; i<=to; i++) {
			int hashed = this.h(i);
			String bin = this.intToBinary(hashed, 20);
			int T = this.tailLength2(bin);
			if (T > max) {
				max = T;
				max_i = i;
			}
		}		
		return max_i;
	}

	public int maxT2(int from, int to, int len) {
		int max = 0;
		int max_i = 0;
		for (int i=from; i<=to; i++) {
			String bin = this.hashAndBinary(i, len);
			int T = this.tailLength2(bin);
			if (T > max) {
				max = T;
				max_i = i;
			}
		}		
		return max_i;
	}
}
public class A12 {

	public static void main(String[] args) {
		
		FM data = new FM();
		// generate data
		int[] data_size = { 10, 100, 1000, 2000, 3000, 5000, 7000, 10000, 30000, 50000, 100000, 300000, 500000, 600000 };
		int length = 20;
		//
		for (int d:data_size) {
			
			int max_i = data.maxT(1, d);
			String max_bin = data.intToBinary( (int)data.h(max_i), length );
			int max = data.tailLength2(max_bin); 
			
			System.out.println("data size: " + d +
					" max: " + max_i + 
					" -> " + data.h(max_i) + 
					" -> "+ max_bin + 
					"\n tail length: " + data.tailLength(max_bin) +
					" / " + max +
					" expected num. of customers 2^T: " + Math.pow(2, max));			
		}
		System.out.println();
		// generate data
		int[] data_size_2 = { 1000, 3000, 5000, 7000, 100000, 300000, 500000, 1000000, 3000000, 5000000, 10000000, 
				30000000, 50000000};
		length = 40;
		//
		for (int d:data_size_2) {
			
			int max_i = data.maxT2(1, d, length);
			String max_bin = data.hashAndBinary(max_i, length);
			int max = data.tailLength2(max_bin); 
			
			System.out.println("data size: " + d +
					" max: " + max_i + 
					" -> "+ max_bin + 
					"\n tail length: " + data.tailLength(max_bin) +
					" / " + data.tailLength2(max_bin) +
					" expected num. of customers 2^T: " + Math.pow(2, max));			
		}
//		
	}

}
